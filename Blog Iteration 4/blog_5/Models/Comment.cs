﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace blog_5.Models
{
    public class Comment
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Bạn không được bỏ trống")]
        [MinLength(50, ErrorMessage = "Bạn phải nhập ít nhất 50 kí tự")]
        public String Body { set; get; }
        [Required(ErrorMessage = "Bạn không được bỏ trống")]
        [DataType(DataType.DateTime, ErrorMessage = "Bạn nhập không đúng ngày tháng")]
        public DateTime DateCreated { set; get; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }

        public int PostID { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        
    }
}