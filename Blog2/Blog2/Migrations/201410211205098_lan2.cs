namespace Blog2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Posts", newName: "BaiViet");
            AlterColumn("dbo.BaiViet", "Body", c => c.String(nullable: false, maxLength: 250));
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Tags", "Content", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "FirstName", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Accounts", "LastName", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts", "LastName", c => c.String());
            AlterColumn("dbo.Accounts", "FirstName", c => c.String());
            AlterColumn("dbo.Accounts", "Email", c => c.String());
            AlterColumn("dbo.Tags", "Content", c => c.String());
            AlterColumn("dbo.Comments", "Body", c => c.String());
            AlterColumn("dbo.BaiViet", "Body", c => c.String(maxLength: 250));
            RenameTable(name: "dbo.BaiViet", newName: "Posts");
        }
    }
}
